import java.util.Locale;
import java.util.ResourceBundle;

public class App
{

	public static void main(String[] args) 
	{
		Locale.setDefault(new Locale("en","US"));
		ResourceBundle bundle=ResourceBundle.getBundle("MyLabels");
		System.out.println("Message in " + Locale.getDefault()+ " : " +bundle.getString("greeting_US"));
		Locale.setDefault(new Locale("en","IN"));
		bundle=ResourceBundle.getBundle("MyLabels");
		System.out.println("Message in " + Locale.getDefault()+ " : " +bundle.getString("greeting_IN"));
	}

}
