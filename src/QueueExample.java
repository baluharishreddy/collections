import java.util.Iterator;
import java.util.ListIterator;
import java.util.PriorityQueue;

public class QueueExample 
{

	public static void main(String[] args)
	{
		PriorityQueue<String> q=new PriorityQueue<String>();
		q.add("Amit");
		q.add("Vijay");
		q.add("Karan");
		q.add("Jai");
		q.add("Rahul");
		System.out.println("head "+ q.element());
		System.out.println("head "+ q.peek());
		System.out.println("Iterating.... ");
		Iterator i=q.iterator();
		while(i.hasNext())
		{
			 System.out.println(i.next());  
		}
		q.remove();
		q.poll();
		System.out.println("After removing two elements.... ");
		Iterator<String> j=q.iterator();
		while(j.hasNext())
		{
			 System.out.println(j.next());  
		}
		
	}

}
