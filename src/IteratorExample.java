import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
public class IteratorExample 
{

	public static void main(String[] args)
	{
		List<String> l=new ArrayList<String>();
		l.add("Balu");
		l.add("Harish");
		l.add("Reddy");	
		//Iterator<String> i=l.iterator();
		ListIterator<String> i=l.listIterator();
		while(i.hasNext())
		{
			 System.out.println(i.next());  
		}
		while(i.hasPrevious())
		{
			 System.out.println(i.previous());  
		}
		
	}
}
