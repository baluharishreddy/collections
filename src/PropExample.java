import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Properties;

public class PropExample
{

	public static void main(String[] args) throws Exception
	{
		FileReader r=new FileReader("db.properties");
		Properties p=new Properties();
		p.load(r);
		System.out.println(p.getProperty("user"));
		System.out.println(p.getProperty("password"));

	}

}
