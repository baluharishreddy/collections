
import java.util.Map;
import java.util.TreeMap;

public class TreeMapExample 
{

	public static void main(String[] args)
	{
		Map<String,Integer> mp=new TreeMap<String,Integer>();
		mp.put("balu", 3);
		mp.put("harish", 6);
		mp.put("reddy", 1);
		mp.put("maddireddy", 4);
		System.out.println(mp);

	}

}
